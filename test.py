import ksp

def test():
    kerbin = ksp.planet.kerbin()
    low_kerbin_orbit = ksp.orbital_trajectory(67e4, 67e4, kerbin)
    low_to_mun_orbit = ksp.orbital_trajectory(67e4, 12e6, kerbin)


    # Step1: launch to low kerbin orbit: deltaV?
    # Step2: go to mun
    to_mun_deltav = low_to_mun_orbit.speed_perapsis() - low_kerbin_orbit.speed_perapsis()
    print("to mun deltaV: ", to_mun_deltav, "m/s")
    # Step2: land on mun: deltaV ? it should be around espace velocity
    # Step3: espace velocity mun
    espace_mun_deltaV = ksp.planet.mun().espace_velocity()
    # Step4: low orbit kerbin: deltaV around same as step 2
    # Step5: land on kerbin: low deltaV

    total_deltaV = 2*to_mun_deltav + 2*espace_mun_deltaV
    # let us build a rocket
    print("total delta V: ", total_deltaV, "m/s")

    # we create a stage containing a parachute, a launch module, and a terrier engine
    last_stage = ksp.stage(ksp.engine.terrier(), [ksp.propellant.t800()], [ksp.payload.mk16parachute(), ksp.payload.mk1()])
    rocket = ksp.rocket([last_stage])
    deltaV1 = rocket.computeDeltaV(last_stage.mass_propellant())
    print("deltaV1: ", deltaV1, "m/s")


if __name__ == '__main__':
    test()
