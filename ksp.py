import math

gravitational_constant = 6.674*1e-11
standard_gravity = 9.80665

class planet:
    def __init__(self, mass, radius):
        self.m_mass = mass
        self.m_radius = radius

    def mass(self):
        return self.m_mass

    def radius(self):
        return self.m_radius

    def espace_velocity(self):
        return math.sqrt(2*gravitational_constant*self.mass()/self.radius())

    def kerbin():
        return planet(5.2915158*1e22, 6*1e5)

    def mun():
        return planet(9.7599066*1e20, 2*1e5)

class engine:
    def __init__(self, mass, isp):
        self.m_mass = mass
        self.m_isp = isp

    def mass(self):
        return self.m_mass

    def isp(self):
        return self.m_isp

    def terrier():
        return engine(0.5e3, 345)

    def swivel():
        return engine(1.5e3, 320)

    def empty():
        return engine(0,0)

class propellant:
    def __init__(self, dry_mass, mass_propellant):
        self.m_dry_mass = dry_mass
        self.m_mass_propellant = mass_propellant

    def mass(self):
        return self.m_dry_mass + self.m_mass_propellant

    def mass_propellant(self):
        return self.m_mass_propellant

    def t200():
        return propellant(0.125e3, 1e3)

    def t400():
        return propellant(0.25e3, 2e3)

    def t800():
        return propellant(0.5e3, 4e3)

class payload:
    def __init__(self, mass):
        self.m_mass = mass

    def mass(self):
        return self.m_mass

    def mk1():
        return payload(0.84e3)

    def mk16parachute():
        return payload(0.1e3)

class stage:
    def __init__(self, engine, propellants, parts):
        self.engine = engine
        self.propellants = propellants
        self.parts = parts

    def mass(self):
        mass = self.engine.mass()
        for part in self.parts:
            mass += part.mass()
        for part in self.propellants:
            mass += part.mass()
        return mass

    def mass_propellant(self):
        mass = 0
        for propellant in self.propellants:
            mass += propellant.mass_propellant()

        return mass

    def burn_propellant(self, quantity):
        for part in self.propellants:
            burnt_quantity = min(part.mass_propellant(), quantity)
            part.m_mass_propellant -= burnt_quantity
            quantity -= burnt_quantity

            if quantity == 0:
                break


class rocket:
# active stage
    def __init__(self, stages):
        self.m_stages = stages
        self.m_active_stage = 0

    def computeDeltaV(self, burnt_propellant):
        total_mass = 0
        deltaV = 0
        for stage in self.m_stages:
            total_mass += stage.mass()

        if(burnt_propellant <= self.m_stages[self.m_active_stage].mass_propellant()):
            total_mass_after = total_mass - burnt_propellant
            deltaV = self.m_stages[self.m_active_stage].engine.isp()*standard_gravity*math.log(total_mass/total_mass_after)

        return deltaV

    def addStage(self, stage):
        self.m_stages.append(stage)

    def optimizeStageForDeltaV(self, deltav):
        return 0

class orbital_trajectory:
    def __init__(self, periapsis, apoapsis, planet):
        self.periapsis = periapsis
        self.apoapsis = apoapsis
        self.planet = planet

    def semi_axis(self):
        return (self.periapsis + self.apoapsis) / 2

    def speed_r(self, r):
        # vis viva equation
        speed2 = gravitational_constant * self.planet.mass() * (2 / r - 1 / self.semi_axis())
        return math.sqrt(speed2)

    def speed_perapsis(self):
        return self.speed_r(self.periapsis)

    def speed_apoapsis(self):
        return self.speed_r(self.apoapsis)

class manoeuvres:
    def __init__(self, orbital_trajectories):
        self.orbital_trajectories = orbital_trajectories

